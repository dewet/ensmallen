CREATE TABLE IF NOT EXISTS short_links (
  short_name TEXT PRIMARY KEY NOT NULL,
  url TEXT NOT NULL,
  title TEXT,
  description TEXT,
  image TEXT,
  interpolate_url_params INTEGER DEFAULT 0,
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
  created_by TEXT,
  clicks INTEGER DEFAULT 0
) WITHOUT ROWID;

CREATE INDEX idx_links_creators ON short_links (created_by);
CREATE INDEX idx_links_clicks ON short_links (clicks);
