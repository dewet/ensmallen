> _Ensmallen_ -- Logical antonym of "embiggen," a humorous neologism coined by television writer David X. Cohen

A tiny python service to shorten URLs (think bit.ly or goo.gl). SAML-based SSO enables easy authentication,
storage is in an embedded SQLite database for maximum compactness, and URL interpolation provides easy templating.

[![build status](https://gitlab.com/dewet/ensmallen/badges/master/build.svg)](https://gitlab.com/dewet/ensmallen/commits/master)
[![coverage report](https://gitlab.com/dewet/ensmallen/badges/master/coverage.svg)](https://gitlab.com/dewet/ensmallen/commits/master)

This project isn't intended to be a public free-for-all service, but is instead to be deployed
internally to an organisation that uses SAML-based SSO to provision users onto web apps.