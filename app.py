#!/usr/bin/env python3
import logging
import os
import sqlite3
from functools import lru_cache
from urllib.parse import urlparse

import requests
from flask import Flask, request, render_template, redirect, g, url_for, session
from flask_bootstrap import Bootstrap
from flask_bootstrap import WebCDN
from flask_login import LoginManager, UserMixin, login_required, login_user, current_user
from raven.contrib.flask import Sentry
from saml2 import BINDING_HTTP_POST, BINDING_HTTP_REDIRECT, entity
from saml2.client import Saml2Client
from saml2.config import Config as Saml2Config
from webpreview import web_preview, InvalidURL
from werkzeug.urls import url_encode

app = Flask(__name__)
app.secret_key = 'something secret'  # replace with your own
HOSTNAME = os.environ.get('HOSTNAME', default='localhost:5000')
PORT = int(os.environ.get('PORT', default=5000))
DATABASE = 'db/database.db'

# Add Flask-Login extension
login_manager = LoginManager()
login_manager.init_app(app)
user_store = {}  # in-memory user cache

# Add Bootstrap extension
Bootstrap(app)
app.extensions['bootstrap']['cdns']['jquery'] = WebCDN('//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/')

# To enable Sentry integration, set your DSN via the SENTRY_DSN environment variable
sentry = Sentry(app, logging=True, level=logging.ERROR)

if not os.environ.get('SAML_METADATA_URL', None):
    raise RuntimeError('Metadata URL for SAML IdP must be specified in SAML_METADATA_URL environment variable.')

if not os.path.exists(DATABASE):
    db = sqlite3.connect(DATABASE)
    with open('schema.sql', mode='r') as f:
        logging.info('Creating initial database schema')
        db.cursor().executescript(f.read())
    db.commit()


class User(UserMixin):
    def __init__(self, user_id):
        self.id = user_id


@login_manager.user_loader
def load_user(user_id):
    return user_store.get(user_id, None)


@lru_cache()
def fetch_metadata() -> str:
    return requests.get(os.environ.get('SAML_METADATA_URL')).text


def saml_client():
    """
    Configure an appropriate SAML client.
    """

    acs_url = url_for("idp_initiated", _external=True)
    https_acs_url = url_for("idp_initiated", _external=True, _scheme='https')

    saml_config = Saml2Config()
    saml_config.load({
        'metadata': {
            'inline': [fetch_metadata()],
        },
        'service': {
            'sp': {
                'endpoints': {
                    'assertion_consumer_service': [
                        (acs_url, BINDING_HTTP_REDIRECT),
                        (acs_url, BINDING_HTTP_POST),
                        (https_acs_url, BINDING_HTTP_REDIRECT),
                        (https_acs_url, BINDING_HTTP_POST)
                    ],
                },
                # Don't verify that the incoming requests originate from us via
                # the built-in cache for authn request ids in pysaml2
                'allow_unsolicited': True,
                # Don't sign authn requests, since signed requests only make
                # sense in a situation where you control both the SP and IdP
                'authn_requests_signed': False,
                'logout_requests_signed': True,
                'want_assertions_signed': True,
                'want_response_signed': False,
            },
        },
    })
    saml_config.allow_unknown_attributes = True
    return Saml2Client(config=saml_config)


@app.route("/saml/sso", methods=['POST'])
def idp_initiated():
    """
    Handle SSO sign-in from the IdP side.
    """
    authn_response = saml_client().parse_authn_request_response(request.form['SAMLResponse'], entity.BINDING_HTTP_POST)
    ava = authn_response.get_identity()
    username = authn_response.get_subject().text

    user = User(username)
    user_store[username] = user
    session['saml_attributes'] = authn_response.ava
    login_user(user)
    # NOTE: On a production system, the RelayState MUST be validated before blindly redirecting!
    # if 'RelayState' in request.form:
    #     return redirect(request.form['RelayState'])
    if 'pre_login_destination' in session:
        return redirect(session['pre_login_destination'])
    return redirect(url_for('home'))


@app.route("/saml/login")
def sp_initiated():
    """
    Initiate SSO sign-in from our (the SP) side.
    """

    _, info = saml_client().prepare_for_authenticate()

    redirect_url = None
    # Select the IdP URL to send the AuthN request to
    for key, value in info['headers']:
        if key is 'Location':
            redirect_url = value
            break
    response = redirect(redirect_url, code=302)
    # Be entirely safe and try to prevent caching of the short-lived login redirect
    response.headers['Cache-Control'] = 'no-cache, no-store'
    response.headers['Pragma'] = 'no-cache'
    return response


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.row_factory = sqlite3.Row
    return db


@app.teardown_appcontext
def close_connection(_):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


@app.route('/healthz')
def healthz():
    return 'ok', 200


@app.route('/')
def home():
    if not current_user.is_authenticated:
        return redirect(url_for('sp_initiated'))
    my_links = query_db('SELECT * FROM short_links WHERE created_by = ?', (current_user.id,))
    return render_template('home.html', links=my_links)


@app.route('/', methods=['POST'])
@login_required
def post_url():
    short_name = request.form.get('short_name')
    url = request.form.get('url')
    interpolate_url_params = bool(request.form.get('interpolate_url_params'))
    if urlparse(url).scheme == '':
        url = 'http://' + url
    try:
        title, description, image = web_preview(url, timeout=5000)
    except InvalidURL:
        title, description, image = '', '', ''
    with get_db() as conn:
        conn.cursor().execute('INSERT INTO short_links'
                              ' (short_name, url, title, description, image, interpolate_url_params, created_by)'
                              ' VALUES (?, ?, ?, ?, ?, ?, ?)', (short_name, url, title, description, image,
                                                                1 if interpolate_url_params else 0,
                                                                current_user.id))
    return render_template('home.html', short_url='%s/%s' % (HOSTNAME, short_name), url=url, title=title,
                           description=description, image=image)


@app.route('/<path:path>')
def redirect_short_url(path: str):
    parts = path.split('/', maxsplit=1)
    short_name = parts[0]
    extra_url_parts = ''
    if len(parts) > 1:
        extra_url_parts = parts[1]

    res = query_db('SELECT url, interpolate_url_params FROM short_links WHERE short_name = ?', (short_name,), one=True)
    if res and res[0]:
        final_url = res[0]
        if len(parts) > 1:
            if res[1]:  # interpolate URL params
                for idx, value in enumerate(extra_url_parts.split('/')):
                    final_url = final_url.replace('%%%%%u%%%%' % idx, value)
            else:
                final_url += '/' + extra_url_parts
        if request.args:
            final_url += '?' + url_encode(request.args)

        with get_db() as conn:
            conn.cursor().execute('UPDATE short_links SET clicks = clicks + 1 WHERE short_name = ?', (short_name,))

        return redirect(final_url)
    return render_template('home.html'), 404


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=PORT, debug=True)
