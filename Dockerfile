FROM python:3.6

RUN apt-get update && \
    apt-get install -y xmlsec1 sqlite3 && \
    apt-get autoremove -y

WORKDIR /app
VOLUME /app/db
ADD app.py requirements.txt schema.sql /app/
ADD templates /app/templates/

RUN ["pip", "install", "-r", "requirements.txt"]

EXPOSE 5000
CMD ["gunicorn", "-b", "0.0.0.0:5000", "app:app"]
